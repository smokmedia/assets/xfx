---
title : "AF-02"
tipo : "refrigerador"
tags : ["cooler"]
categories : ["refrigeracion"]

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

carrusel :
 - imga : "../../img/productos/refrigeracion/cooler af02/af02.jpg"
   numero : "1"

 - imga : "../../img/productos/refrigeracion/cooler af02/af02-1.jpg"
   numero : "2"

 - imga : "../../img/productos/refrigeracion/cooler af02/af02-2.jpg"
   numero : "3"

 - imga : "../../img/productos/refrigeracion/cooler af02/af02-3.jpg"
   numero : "4"

 - imga : "../../img/productos/refrigeracion/cooler af02/af02-4.jpg"
   numero : "5"

 - imga : "../../img/productos/refrigeracion/cooler af02/af02-5.jpg"
   numero : "6"
   
 - imga : "../../img/productos/refrigeracion/cooler af02/af02-6.jpg"
   numero : "7"

 - imga : "../../img/productos/refrigeracion/cooler af02/af02-7.jpg"
   numero : "8"


imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "Nuestros Coolers Kaze, como los japoneses llaman al viento, están diseñados específicamente para mantener tu hardware refrigerado a una temperatura óptima para que su funcionamiento se desarrolle con total normalidad, sin contratiempos. Combinados con nuestros gabinetes, estos coolers son la mejor opción para explotar al máximo el rendimiento de tu equipo sin temor al sobrecalentamiento de los componentes. "

imgf : "/img/productos/refrigeracion/cooler af02/af02portada.png"

iconos:
 - img : "../../img/iconos/rgb.png"
   description : "16m colores"
 - img : "../../img/iconos/fan.png"
   description : "Fan de 120 Milimetros"
 - img : "../../img/iconos/1200rpm.png"
   description : "	± %10 "
   
---

#### Especificaciones: ####
 - Long. cable: 500mm +/- 10mm 
 - Velocidad del ventilador: 1200RPM +/- 10%
 - Voltaje: DC12V + 5V 
 - Flujo de aire: 33,5 CFM +/- 10% 
 - Corriente nominal: 0.25A 
 - Tipo de rodamiento: dinámico fluido
 - Tipo de conexión: MOLEX 4 pines 
 - Nivel de ruido: 23dBA +/- 10%git 
 - Promedio de vida útil: 20.000 hs
 - Tope de goma antivibratorio 
 - Incluye 4 tornillos de sujeción