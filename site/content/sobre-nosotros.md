---
title: Sobre Nosotros
description: Aquí encontrarás datos de nuestros inicios

---
### Nuestra Filosofía

Jugar Videojuegos es una actividad que solo aquellos que la practicamos la entendemos. En tiempos pasados, el hábito de jugar videojuegos se reduce a un grupo determinado de personas de una comunidad exclusiva. Con el correr de los años, esta comunidad fue mutando una causa del interés de más personas que encontraron en los videojuegos distensión, divertimento, y pasión. Hoy en día no hay edad ni género y el número de aficionados a esta actividad va en aumento constante.

A través de los videojuegos se viven historias tan reales como fantásticas, emociones que van desde la felicidad hasta el llanto y la furia. Insertados en este mundo fantástico, los usuarios pueden convertirse en un gran hechicero, un guerrero mitológico, un ser de otro planeta, un superhéroe, un soldado y hasta un simple hombre o mujer con una historia divertida. Las posibilidades y las vidas son infinitas, nada se termina. Si se pierde, se aprende y se vuelve a intentar. En eso radica la maravilla de los videojuegos.

Los videojuegos se han abierto paso en el mundo de los deportes imponiéndose como una disciplina deportiva y dando paso a una nueva comunidad: los deportistas electrónicos. Estos expertos han desarrollado habilidades para la competencia que cualquier aficionado desearía tener. En el universo de los videojuegos, los deportistas electrónicos son idolatrados y muchos quieren parecerse a ellos. Estos expertos han desarrollado habilidades para la competencia que cualquier aficionado desearía tener.

Pero, ¿Qué es lo que le hace falta a un Gamer, para alcanzar un nivel de juego superior?

Más allá de tener mucho tiempo libre para ejercitar, se necesita tecnología de alta gama que permita explotar al máximo la experiencia de juego y responder con efectividad a las exigencias con las que cuentan los deportes electrónicos.

En este contexto de desarrollo y evolución, XFX desarrolla y brinda productos pensados ​​específicamente para todo tipo de videojugadores.

La marca XFX está inspirada en la cultura Samurái, adaptando los valores expresados ​​en el Bushido (código Samurái). Desde XFX transmitimos ORGULLO por nuestros productos y promovemos el HONOR de ser elegidos por los usuarios que buscan satisfacer sus preferencias a la hora enfrentar una partida. La SINCERIDAD es la clave en una relación de confianza que brindamos a través de la calidad de los productos, así como también la CORTESÍA con el cual nos dirigimos a nuestros usuarios transmitiendo empatía recíproca. Afrontamos nuestro trabajo con el DEBER de ofrecer la mejor calidad en nuestros productos y anteponer la LEALTAD que debemos a aquellos con quienes compartimos nuestra pasión por los videojuegos.

¡Este es nuestro acuerdo, nuestro PACTO DE SANGRE con el mundo gaming!

Con años de experiencia y retornando a Argentina, en XFX ofrecemos una gran variedad de productos generados importantes para que la experiencia del jugador se maximice y se exprese en todo su esplendor. Innovando y haciendo uso de tecnología de punta, ofrecemos soluciones tecnológicas apuntadas a todo tipo de deportistas electrónicos, desde los más exigentes, enfocados a desarrollarse en el mundo de los Esports, como aquellos jugadores casuales que quizás no buscan competir, pero se interesan por disfrutar de una buena historia al 100%. De esta forma, buscamos impactar de manera positiva en la experiencia y el entretenimiento de todos aquellos amantes del gaming.

Se necesita mucho poder para generar gráficos con una definición tan evolucionada como la de hoy en día, para disminuir esa latencia entre comandos, acciones y refresco. Es justamente en estos factores en los que nos especializamos, para brindar la mejor experiencia para que nuestros clientes vivan sus vidas al máximo, se vuelvan más competitivos para que sus competidores los tomen en serio.

XFX te ofrece la oportunidad para que puedas vencer a ese _boss_ de gran nivel, desarrolles todo tu potencial y explotes todo tu poder, este es el poder que XFX te da, este es el poder que XFX tiene.

XFX GET THE GAMING POWER