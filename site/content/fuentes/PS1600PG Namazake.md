---
title : "PS1600PG"
tipo : "fuente"
tags : ["namasake"]
categories : ["fuente"]

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

cuadros :
  - cuadro1 : "../../img/productos/fuentes/fuente1600/cuadro21600w.svg"
    cuadro2 : "../../img/productos/fuentes/fuente1600/cuadro1600w.svg"

carrusel :
 - imga : "../../img/productos/fuentes/fuente1600/namazake1600portadagold.jpg"
   numero : "1"

 - imga : "../../img/productos/fuentes/fuente1600/namazake1600-1gold.jpg"
   numero : "2"

 - imga : "../../img/productos/fuentes/fuente1600/namazake1600-2gold.jpg"
   numero : "3"

 - imga : "../../img/productos/fuentes/fuente1600/namazake1600-3gold.jpg"
   numero : "4"

 - imga : "../../img/productos/fuentes/fuente1600/namazake1600-4gold.jpg"
   numero : "5"

 - imga : "../../img/productos/fuentes/fuente1600/namazake1600-5gold.jpg"
   numero : "6"

 - imga : "../../img/productos/fuentes/fuente1600/namazake1600-6gold.jpg"
   numero : "7"


imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "El sake es una bebida alcohólica originaria de Japón que se genera a partir de la destilación del grano de arroz. La diferencia entre el sake tradicional y el Namazake, radica en el proceso de pasteurización. Mientras que el sake tradicional se pasteuriza 2 veces, el namazake no se pasteuriza y permite que el sabor sea más fresco. Antiguamente, existía el mito que el sake proporcionaba a los samuráis la fortaleza y la energía suficientes para el combate. 
Nuestras fuentes Namakaze, toman las propiedades mitológicas del sake para crear una fuente que proporciona la energía suficiente para el mejor funcionamiento de tu equipo."

imgf : "/img/productos/fuentes/fuente1600/namazake1600portadagold.png"

iconos:
 - img : "../../img/iconos/premium.png"
   description : "Fácil manipulación"
 - img : "../../img/iconos/proteccion.png"
   description : "Protecciones de seguridad"
 - img : "../../img/iconos/140mm.png"
   description : "Fan"
 - img : "../../img/iconos/220.png"
   description : "220/240 AC"
 - img : "../../img/iconos/12v.png"
   description : "Riel único"
 - img : "../../img/iconos/white.png"
   description : "Certificiado"
   
---
- Ventilador de 140mm con Alto rendimiento

- Diseño de cables de fácil manipulación

- Múltiple protección de seguridad OPP/UVP/OVP/SCP

- +89% Eficiencia energética par un óptimo rendimiento energético

80 PLUS Gold es la 3ª mejor certificación energética, caracterizada por ofrecer las siguiente eficiencia en los distintos niveles de carga:

20% -> 90%.
50% -> 92%.
100% -> 89%.

¿Qué tiene de especial? Se puede decir que es la certificación energética perfecta en relación calidad-precio, el «BBB» de toda la vida. Esto se debe a que los precios de las fuentes de alimentación 80 PLUS Gold no es bajo, pero tampoco alto. Por ello, muchos consumidores se lanzan directamente a esta certificación energética.

- 220-240v Ac input y puede trabajar a 220V y a 240V

- Salida única de 12V aplicada a otorgar mayor potencia y estabilidad

-Full Modular