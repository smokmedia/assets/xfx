---
title : "PS650PW"
tipo : "fuente"
tags : ["namasake"]
categories : ["fuente"]

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

cuadros :
  - cuadro1 : "../../img/productos/fuentes/fuente650/cuadro2650w.svg"
    cuadro2 : "../../img/productos/fuentes/fuente650/cuadro650w.svg"

carrusel :
 - imga : "../../img/productos/fuentes/fuente650/namazake650.jpg"
   numero : "1"

 - imga : "../../img/productos/fuentes/fuente650/namazake650-1.jpg"
   numero : "2"

 - imga : "../../img/productos/fuentes/fuente650/namazake650-2.jpg"
   numero : "3"
   
 - imga : "../../img/productos/fuentes/fuente650/namazake650-3.jpg"
   numero : "4"

 - imga : "../../img/productos/fuentes/fuente650/namazake650-4.jpg"
   numero : "5"

 - imga : "../../img/productos/fuentes/fuente650/namazake650-5.jpg"
   numero : "6"

 - imga : "../../img/productos/fuentes/fuente650/namazake650-6.jpg"
   numero : "7"

 - imga : "../../img/productos/fuentes/fuente650/namazake650-7.jpg"
   numero : "8"


imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "El sake es una bebida alcohólica originaria de Japón que se genera a partir de la destilación del grano de arroz. La diferencia entre el sake tradicional y el Namazake, radica en el proceso de pasteurización. Mientras que el sake tradicional se pasteuriza 2 veces, el namazake no se pasteuriza y permite que el sabor sea más fresco. Antiguamente, existía el mito que el sake proporcionaba a los samuráis la fortaleza y la energía suficientes para el combate. 
Nuestras fuentes Namakaze, toman las propiedades mitológicas del sake para crear una fuente que proporciona la energía suficiente para el mejor funcionamiento de tu equipo."

imgf : "/img/productos/fuentes/fuente650/namazake650portada.png"

iconos:
 - img : "../../img/iconos/premium.png"
   description : "Fácil manipulación"
 - img : "../../img/iconos/proteccion.png"
   description : "Protecciones de seguridad"
 - img : "../../img/iconos/120mm.png"
   description : "Fan"
 - img : "../../img/iconos/220.png"
   description : "220/240 AC"
 - img : "../../img/iconos/12v.png"
   description : "Riel único"
 - img : "../../img/iconos/white.png"
   description : "Certificiado"
   
---
- Ventilador de 120mm con Alto rendimiento

- Diseño de cables de fácil manipulación

- Múltiple protección de seguridad OPP/UVP/OVP/SCP

- +80% Eficiencia energética par un óptimo rendimiento energético

- 220-240v Ac input y puede trabajar a 220V y a 240V

- Salida única de 12V aplicada a otorgar mayor potencia y estabilidad
