+++
banner = "/uploads/gwen-nuevo-personaje.jpg"
categories = ["Gaming"]
date = 2021-04-13T03:00:00Z
description = "El LOL (League Of Legends), es un videojuego multijugador de 5 vs 5 en línea para PC que tiene como objetivo destruir el nexo o base enemiga..."
tags = ["League Of Legends", "MOBA", "PC Gaming", "lol"]
title = "League of Legends: el MOBA de Riot Games que al día de hoy sigue dando que hablar"

+++
![](/uploads/gwen-nuevo-personaje.jpg)

### **League of Legends: el MOBA de Riot Games que al día de hoy sigue dando que hablar**

El LOL (League Of Legends), es un videojuego multijugador en línea de 5 vs 5 para PC, su objetivo es destruir el nexo o base enemiga. Antes de alcanzar el objetivo, el equipo debe atravesar varios desafíos como conseguir la mayor cantidad de dragones (atacantes a distancia que pueden causar daño a múltiples campeones a la vez), destruir torretas enemigas y también alcanzar un armamento adecuado.

**El próximo 14 de abril llega el parche 11.8**

En primer lugar comenzaremos con lo fundamental de este parche, los _buffeos_ (atributo positivo que se le otorga a un personaje y que le otorga una ventaja frente a los demás) y los _nerffeos_ (implica reducir el atractivo, la potencia o las posibilidades de un objeto, personaje o cualquier otro elemento). Uno de los _nerfs_ más importantes en esta actualización es el del personaje _Yorick_ que en el parche anterior fue _buffeado_ considerablemente. También entran _Orianna_, _Gnar_ y _Thresh_. Por el lado de los _buffeos,_ el que tendrá más aumento será _Aphelios,_ mientras que con cambios más pequeños entran _Lee Sin_, _LeBlanc_ y _Zac_. Por último, llega un nuevo personaje llamado “_Gwen_”, [en este link](https://www.youtube.com/watch?v=xtLY0Rm7sF4) te dejamos el video de presentación de esta "_costurera sagrada"_.

¿Buscás comodidad a la hora de jugar al LOL?

En XFX tenemos la opción perfecta para vos, nuestro _Kit Kake_ es un combo de periféricos (teclado, mouse, auricular y pad) que reúne en un solo conjunto todo lo necesario para que te destaques en tus partidas, especialmente cuando tengas que farmear o entrar en una frenética batalla con el equipo enemigo. En [este link](https://xfx.com.ar/perifericos/gpk-01-kake/) te dejamos toda la información para que conozcas las especificaciones técnicas de cada uno.

XFX Get The Gaming Power.