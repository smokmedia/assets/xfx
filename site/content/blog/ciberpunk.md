+++
title = "Ciberpunk 2077: acción, aventuras, Keanu Reeves y mucho pero mucho metal"
date = "2021-01-07T13:50:46+02:00"
tags = ["ciberpunk"]
categories = ["gaming"]
description = "Después de un sinfín de retrasos el titulo de CD Proyect red por fin pudo ver la luz, aunque con más problemas que de costumbre, lleno de errores que ni las consolas de última generación pudieron soportar"
banner = "img/banners/Cyberpunk-2077.jpg"
+++

![](/img/banners/Cyberpunk-2077.jpg)

### Ciberpunk 2077: acción, aventuras, Keanu Reeves y mucho pero mucho metal

Después de un sinfín de retrasos el titulo de CD Proyect red por fin pudo ver la luz, aunque con más problemas que de costumbre, lleno de errores que ni las consolas de última generación pudieron soportar. Pero si eres un afortunado amante del PC Gaming, no hay de qué preocuparse siempre y cuando tu equipo lo soporte. Porque verdaderamente este juego pide unos requerimientos mínimos bastante exagerados.

Cyberpunk 2077 es una historia de acción y aventura de mundo abierto ambientada en la megalópolis futurista llamada Night City. Una ciudad obsesionada por el poder y llena de glamour, donde el último grito de la moda son los implantes y la modificación cibernética corporal.
En esta historia encarnaremos a V, un mercenario que persigue un implante único que le permitiría alcanzar la inmortalidad.

Este alocado titulo de género FPS presenta un reto de lo mas frenético, como nos tiene acostumbrada la gente de CD Proyect red, en donde tus habilidades con la armas marcaran tu destino. Necesitaras equiparte de la mejor manera posible para superar el reto que se te presenta en el juego. Y que mejor manera de afrontarlo que con nuestro MOUSE KUNAI GM-01 con el cual obtendrás una Precisión Milimétrica gracias su Sensor óptico de 10.000 DPI, además de poseer un grip anatómico especialmente diseñado para esas largas jornadas Gaming.

[Acá te dejamos un enlace para que puedas ver el trailer oficial del juego](https://www.youtube.com/watch?v=P6RbHOeZS-Q)
