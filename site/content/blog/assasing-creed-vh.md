+++
title = "Assassin 's Creed Valhalla: La Gran Mitología Nórdica."
date = "2021-03-03T13:50:46+02:00"
tags = ["call-of-duty"]
categories = ["gaming"]
description = "Finalmente la nueva entrega de Call of Duty está entre nosotros, y en esta ocasión los muchachos de Treyarch y Raven nos harán revivir la Guerra Fría como nunca antes se ha visto."
banner = "img/banners/assassing-vh.jpg"
+++

![](/img/banners/assassing-vh.jpg)

### Assassin 's Creed Valhalla: La Gran Mitología Nórdica

En esta nueva entrega nos transportamos a Noruega en el siglo IX, en donde los altos picos nevados serán el escenario y los saqueos a aldeas, moneda corriente para obtener mejores recompensas. En esta saga los vikingos, a costa de hacha y escudo, irán en busca de duros enfrentamientos contra los sajones. Un juego que sin lugar a dudas te atrapará desde el principio.

Ubisoft nos hará encarnar a Eivor del Clan Cuervo, que curiosamente tiene el mismo nombre independientemente del género que elijamos. La historia se centra en el asentamiento de Ravensthorpe, el cual tendremos que mejorar e ir sumando nuevas regiones a nuestro mapa. Este asentamiento funciona como un punto crucial en el crecimiento de los vikingos en tierras inglesas.

El juego en sí, tiene un modo historia único, pero lo interesante es visitar todos los puntos que veamos en el mapa, ya que de esa manera nos encontraremos con una serie de curiosas misiones secundarias y puzzles que son la verdadera "joyita" de esta entrega.

Desde XFX entendemos que gran parte de nuestro público son personas rigurosas con la calidad del videojuego, y en este caso no es la excepción ya que la calidad gráfica, música y demás, son dignos de apreciar, y por eso te recomendamos nuestros gabinetes KEIKO y YOROI que cuentan con un diseño específicamente pensado para que la refrigeración de tu sistema sea óptima, para disfrutar a pleno de esta nueva aventura sin contratiempos.

XFX. Get The Gaming Power.

[Gameplay](https://www.youtube.com/watch?v=SrwKP2iTPb8&feature=emb_title)