---
title: GC-03HE
tipo: gabinete
modelo: yoroi
tags:
- yoroi
categories:
- gabinete
mostrar: 'no'
carrusel:
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03.jpg"
  numero: "1"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03-1.jpg"
  numero: "2"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03-2.jpg"
  numero: "3"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03-3.jpg"
  numero: "4"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03-4.jpg"
  numero: "5"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03-5.jpg"
  numero: "6"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03-6.jpg"
  numero: "7"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03-7.jpg"
  numero: "8"
imagenes:
- ruta: "../img/gabinete_slider.jpg"
- ruta: "../img/gabinete_slider.jpg"
descripcion: Gabinetes de alta gama inspirados en la armadura Samurái que lleva su
  nombre (Yoroi). Ésta línea de productos es la evolución de la gama Keiko. Además
  de poseer la protección necesaria para el hardware de tu PC, poseen características
  estéticas que los hacen únicos y singulares para adaptarse al gusto del usuario.
  Las armaduras Yoroi fueron utilizadas durante el Periodo Edo (posterior al Sengoku,
  período de Guerra). Esta armadura era portada por los Samuráis de alto rango, proporcionando
  la misma protección que las Keiko, estas se caracterizaban por ser más ligeras y
  brindaban mayor libertad de movimiento a quien la usara, a su vez, dependiendo del
  status a cual perteneciera el Samurái, su ornamenta y colores eran distintos, eso
  era una de las formas para diferenciar a cada clan.
imgf: "/img/productos/gabinetes/yoroi/gabinete GC-03HE yoroi/YoroiGC03portada.png"
iconos:
- img: "../../img/iconos/aire.png"
  description: MAX-168mm
- img: "../../img/iconos/filtro.png"
  description: Filtro antipolvo
- img: "../../img/iconos/placadevideo.png"
  description: MAX-370mm
- img: "../../img/iconos/sonido.png"
  description: Jack 3.5mm
- img: "../../img/iconos/ssd.png"
  description: Compatible
- img: "../../img/iconos/water.png"
  description: Compatible
- img: "../../img/iconos/usb gabo.png"
  description: 3.0/2.0
- img: "../../img/iconos/fans.png"
  description: 6 RGB FAN
- img: "../../img/iconos/vidriotemplado.png"
  description: Vidrio templado

---
#### Compatibilidad:

* Entrada micrófono / auriculares
* Filtro Antipolvo
* USB 2.0 / 3.0
* Vidrio templado
* Compatible con SSD
* Placas de video de 370mm
* Refrigeración líquida: 120 / 240mm y 140 / 280mm
* Límite de CPU: Máx 168 mm
* 6 Ventiladores RGB
* Tipo de fuente: ATX
* Filtro antipolvo superior: Sí (Magnético)
* Posición de la fuente: Inferior
* Refrigerador frontal: 3 x 120mm (Incluidos - Con RGB controlable)
* Refrigerador posterior: 1 x 120mm (Incluidos - Con RGB controlable)
* Cooler superior: 2 x 120mm (Incluidos - Con RGB controlable)
* Filtro de la fuente: Sí (Encastrable)
* Filtro Frontal: Sí (Fijo)
* Póster Base 4 Pines antideslizantes
* Vidrio templado rontal: Sí
* Vidrio templado lateral izquierdo: Sí (Con apoyo antivibratorio)