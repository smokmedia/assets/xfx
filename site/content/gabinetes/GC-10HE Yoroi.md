---
title : "GC-10HE"
tipo : "gabinete"
modelo : "yoroi"
tags : ["yoroi"]
categories : ["gabinete"]

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

carrusel :
 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10.jpg"
   numero : "1"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10-1.jpg"
   numero : "2"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10-2.jpg"
   numero : "3"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10-3.jpg"
   numero : "4"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10-4.jpg"
   numero : "5"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10-5.jpg"
   numero : "6"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10-6.jpg"
   numero : "7"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10-7.jpg"
   numero : "8"


imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "Gabinetes de alta gama inspirados en la armadura Samurái que lleva su nombre (Yoroi). Ésta línea de productos es la evolución de la gama Keiko. Además de poseer la protección necesaria para el hardware de tu PC, poseen características estéticas que los hacen únicos y singulares para adaptarse al gusto del usuario.
Las armaduras Yoroi fueron utilizadas durante el Periodo Edo (posterior al Sengoku, período de Guerra). Esta armadura era portada por los Samuráis de alto rango, proporcionando la misma protección que las Keiko, estas se caracterizaban por ser más ligeras y brindaban mayor libertad de movimiento a quien la usara, a su vez, dependiendo del status a cual perteneciera el Samurái, su ornamenta y colores eran distintos, eso era una de las formas para diferenciar a cada clan."

imgf : "/img/productos/gabinetes/yoroi/gabinete GC-10he yoroi/YoroiGC10portada.png"

iconos:
- img: "../../img/iconos/aire.png"
  description: MAX-165mm
- img: "../../img/iconos/filtro.png"
  description: Filtro antipolvo
- img: "../../img/iconos/placadevideo.png"
  description: MAX-430mm
- img: "../../img/iconos/sonido.png"
  description: Jacks 3.5mm
- img: "../../img/iconos/ssd.png"
  description: Compatible
- img: "../../img/iconos/water.png"
  description: Compatible
- img: "../../img/iconos/usb gabo.png"
  description: 3.0/2.0
- img: "../../img/iconos/fans.png"
  description: 3 RGB FAN
- img: "../../img/iconos/vidriotemplado.png"
  description: Vidrio templado

---
#### Compatibilidad:

* Entrada micrófono / auriculares
* Filtro Antipolvo
* USB 2.0 / 3.0
* Compatible con SSD
* Placas de video de 430mm
* Refrigeración líquida: Frontal: 240 / 360mm Lateral: 240mm Superior: 240 / 360mm Posterior: 120mm
* Límite de CPU: Máx 165 mm
* Tipo de fuente: ATX
* Posición de la fuente: inferior
* Medidas: alto 510cm, ancho 225cm y lago 478cm
* Disco duro: 2
* SSD: 4/6
* Frigorífico frontal: 3 x 120 mm (RGB instalados)
* Refrigerador posterior: 1 x 120 mm (RGB instalado)
* Refrigerador superior: 3 x 120mm (no incluidos)
* Refrigerador lateral: 2 x 120mm (no incluidos)
* Filtro antipolvo superior e inferior magnético
* Vidrio lateral derecho: SI
* Vidrio lateral izquierdo: SI
* Base: pies dobles
* Controlador RGB y control remoto