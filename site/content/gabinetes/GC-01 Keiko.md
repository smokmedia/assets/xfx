---
title: GC-01
tipo: gabinete
modelo: keiko
tags:
- Keiko
categories:
- gabinete
mostrar: 'no'
carrusel:
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01.jpg"
  numero: "1"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01-1.jpg"
  numero: "2"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01-2.jpg"
  numero: "3"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01-3.jpg"
  numero: "4"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01-4.jpg"
  numero: "5"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01-5.jpg"
  numero: "6"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01-6.jpg"
  numero: "7"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01-7.jpg"
  numero: "8"
imagenes:
- ruta: "../img/gabinete_slider.jpg"
- ruta: "../img/gabinete_slider.jpg"
descripcion: La armadura Keiko es una armadura lamelar pre Samurái utilizada en Japón
  alrededor del siglo V (Era Sengoku). Compuesta por una gran cantidad de piezas de
  cuero laqueado y/o hierro entrelazadas, y unidas por cuerda en tiras horizontales.
  Este tipo de armadura se utilizó en el ejército durante el Japón feudal por proporcionar
  una gran protección a su portador.
imgf: "/img/productos/gabinetes/keiko/gabinete GC-01 keiko/KeikoGC01portada.png"
iconos:
- img: "../../img/iconos/aire.png"
  description: MAX-160mm
- img: "../../img/iconos/filtro.png"
  description: Filtro antipolvo
- img: "../../img/iconos/placadevideo.png"
  description: MAX-300mm
- img: "../../img/iconos/sonido.png"
  description: Jacks 3.5mm
- img: "../../img/iconos/ssd.png"
  description: Compatible
- img: "../../img/iconos/water.png"
  description: Compatible
- img: "../../img/iconos/usb gabo.png"
  description: 3.0/2.0
- img: "../../img/iconos/vidriotemplado.png"
  description: Vidrio templado

---
Esta línea de gabinetes fue creada con la inspiración de la armadura Samurái que lleva su nombre. Mediante la utilización de materiales nobles, combinados entre sí, logramos crear un gabinete resistente que se adapta perfectamente a cualquier uso, y proporciona una seguridad óptima a los diferentes componentes de hardware de tu sistema.

#### Compatibilidad:

* Entrada micrófono / auriculares
* Filtro Antipolvo superior: Sí (Magnético)
* Filtro antipolvo de la fuente: Sí (encastrable)
* USB 2.0 / 3.0
* Vidrio templado
* Compatible con 1 SSD y 1 HDD
* Placas de video de 300mm
* Refrigeración líquida: 120 / 240mm
* Límite de CPU: Máx 160 mm
* LED RGB
* Tipo de fuente: ATX (con apoyo antivibratorio)
* Posición de la fuente: Inferior
* Refrigerador posterior: 1 x 80 / 120mm (No incluido)
* Refrigerador superior: 2 x 120 mm (No incluido)
* Filtro inferior: Si - Encastrable (Fuente)
* Vidrio templado lateral izquierdo: SI
* Base 4 Pines antideslizantes