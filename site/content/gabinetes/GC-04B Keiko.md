---
title: GC-04B
tipo: gabinete
modelo: keiko
tags:
- keiko
categories:
- gabinete
mostrar: 'no'
carrusel:
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04.jpg"
  numero: "1"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04-1.jpg"
  numero: "2"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04-2.jpg"
  numero: "3"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04-3.jpg"
  numero: "4"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04-4.jpg"
  numero: "5"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04-5.jpg"
  numero: "6"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04-6.jpg"
  numero: "7"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04-7.jpg"
  numero: "8"
imagenes:
- ruta: "../img/gabinete_slider.jpg"
- ruta: "../img/gabinete_slider.jpg"
descripcion: Esta línea de gabinetes fue creada con la inspiración de la armadura
  Samurái que lleva su nombre. Mediante la utilización de nobles materiales, combinados
  entre sí, logramos crear un gabinete resistente que se adapta perfectamente a cualquier
  uso, y proporciona una seguridad óptima a los diferentes componentes de hardware
  de tu sistema. La armadura Keiko es una armadura lamelar pre Samurái utilizada en
  Japón alrededor del siglo V (Era Sengoku). Compuesta por una gran cantidad de piezas
  de cuero laqueado y/o hierro entrelazadas, y unidas por cuerda en tiras horizontales.
  Este tipo de armadura se utilizó en el ejército durante el Japón feudal por proporcionar
  una gran protección a su portador.
imgf: "/img/productos/gabinetes/keiko/gabinete GC-04 keiko/KeikoGC04portada.png"
iconos:
- img: "../../img/iconos/aire.png"
  description: MAX-175mm
- img: "../../img/iconos/filtro.png"
  description: Filtro antipolvo
- img: "../../img/iconos/placadevideo.png"
  description: MAX-360mm
- img: "../../img/iconos/sonido.png"
  description: Jacks 3.5mm
- img: "../../img/iconos/ssd.png"
  description: Compatible
- img: "../../img/iconos/water.png"
  description: Compatible
- img: "../../img/iconos/usb gabo.png"
  description: 3.0/2.0
- img: "../../img/iconos/fans.png"
  description: 8 FAN
- img: "../../img/iconos/vidriotemplado.png"
  description: Vidrio templado

---
#### Compatibilidad:

* Entrada micrófono / auriculares
* USB 2.0 / 3.0
* Vidrio templado
* Compatibilida de disco: 2 SSD y 2 HDD
* Placas de video de 360mm
* Refrigeración líquida: 120 / 240mm y 140/280 mm
* Límite de CPU: Máx 175 mm
* LED RGB
* Tipo de fuente: ATX
* Filtro antipolvo superior: Sí (Magnético)
* Filtro de la fuente: Sí (Encastrable)
* Filtro Frontal: No
* Posición de la fuente: Inferior
* Frigorífico frontal: 3 x 120 mm (no incluidos)
* Nevera inferior: 2 x 120mm (no incluidos)
* Refrigerador posterior: 1 x 120 mm (no incluido)
* Refrigerador superior: 2 x 120/140 mm (no incluidos)
* Vidrio Frontal: No
* Vidrio lateral: Sí (Con apoyo antivibratorio)
* Base 4 Pines antideslizantes