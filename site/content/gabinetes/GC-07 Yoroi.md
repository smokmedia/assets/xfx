---
title : "GC-07"
tipo : "gabinete"
modelo : "yoroi"
tags : ["yoroi"]
categories : ["gabinete"]

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

carrusel :
 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07.jpg"
   numero : "1"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07-1.jpg"
   numero : "2"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07-2.jpg"
   numero : "3"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07-3.jpg"
   numero : "4"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07-4.jpg"
   numero : "5"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07-5.jpg"
   numero : "6"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07-6.jpg"
   numero : "7"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07-7.jpg"
   numero : "8"

imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "Gabinetes de alta gama inspirados en la armadura Samurái que lleva su nombre (Yoroi). Ésta línea de productos es la evolución de la gama Keiko. Además de poseer la protección necesaria para el hardware de tu PC, poseen características estéticas que los hacen únicos y singulares para adaptarse al gusto del usuario.
Las armaduras Yoroi fueron utilizadas durante el Periodo Edo (posterior al Sengoku, período de Guerra). Esta armadura era portada por los Samuráis de alto rango, proporcionando la misma protección que las Keiko, estas se caracterizaban por ser más ligeras y brindaban mayor libertad de movimiento a quien la usara, a su vez, dependiendo del status a cual perteneciera el Samurái, su ornamenta y colores eran distintos, eso era una de las formas para diferenciar a cada clan."

imgf : "/img/productos/gabinetes/yoroi/gabinete GC-07 yoroi/YoroiGC07portada.png"

iconos:
 - img : "../../img/iconos/aire.png"
   description : "MAX-165mm"
 - img : "../../img/iconos/filtro.png"
   description : "Filtro antipolvo"
 - img : "../../img/iconos/placadevideo.png"
   description : "MAX-320mm"
 - img : "../../img/iconos/sonido.png"
   description : "Jacks 3.5mm"
 - img : "../../img/iconos/ssd.png"
   description : "Compatible"
 - img : "../../img/iconos/water.png"
   description : "Compatible"
 - img : "../../img/iconos/usb gabo.png"
   description : "3.0/2.0"
 - img : "../../img/iconos/fans.png"
   description : "3 RGB FAN"
 - img : "../../img/iconos/vidriotemplado.png"
   description : "Vidrio templado"
   
---
#### Compatibilidad: ####
- Entrada micrófono/auriculares
- Filtro Antipolvo
- USB 2.0/3.0
- SSD compatible
- Placas de video de 320mm
- Refrigeración líquida: 120/240mm
y 140/280mm
- CPU limit: Máx 165 mm
- Tipo de fuente: ATX
- Filtro antipolvo superior: Sí (Magnético)
- Filtro de la fuente: Sí (Encastrable)
- Filtro Frontal: No
- Posición de la fuente: Inferior
- Cooler frontal: 2 x 180 mm (RGB
instalados)
- Cooler inferior: 2 x 120mm (no
incluidos)
- Cooler posterior:: 1 x 120 mm (Incluidos con RGB Controlable)
- Cooler superior: 2 *120/140 mm
(no incluidos)
- Vidrio Frontal: Sí (Con apoyo antivibratorio)
- Vidrio lateral: Sí (Con apoyo antivibratorio)
- Base 4 Pines antideslizantes