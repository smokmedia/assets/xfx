---
title : "GC-09HE"
tipo : "gabinete"
modelo : "yoroi"
tags : ["yoroi"]
categories : ["gabinete"]

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

carrusel :
 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09.jpg"
   numero : "1"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09-1.jpg"
   numero : "2"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09-2.jpg"
   numero : "3"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09-3.jpg"
   numero : "4"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09-4.jpg"
   numero : "5"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09-5.jpg"
   numero : "6"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09-6.jpg"
   numero : "7"

 - imga : "../../img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09-7.jpg"
   numero : "8"

imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "Gabinetes de alta gama inspirados en la armadura Samurái que lleva su nombre (Yoroi). Ésta línea de productos es la evolución de la gama Keiko. Además de poseer la protección necesaria para el hardware de tu PC, poseen características estéticas que los hacen únicos y singulares para adaptarse al gusto del usuario.
Las armaduras Yoroi fueron utilizadas durante el Periodo Edo (posterior al Sengoku, período de Guerra). Esta armadura era portada por los Samuráis de alto rango, proporcionando la misma protección que las Keiko, estas se caracterizaban por ser más ligeras y brindaban mayor libertad de movimiento a quien la usara, a su vez, dependiendo del status a cual perteneciera el Samurái, su ornamenta y colores eran distintos, eso era una de las formas para diferenciar a cada clan."

imgf : "/img/productos/gabinetes/yoroi/gabinete GC-09 yoroi/YoroiGC09portada.png"

iconos:
- img: "../../img/iconos/aire.png"
  description: MAX-165mm
- img: "../../img/iconos/filtro.png"
  description: Filtro antipolvo
- img: "../../img/iconos/placadevideo.png"
  description: MAX-430mm
- img: "../../img/iconos/sonido.png"
  description: Jacks 3.5mm
- img: "../../img/iconos/ssd.png"
  description: Compatible
- img: "../../img/iconos/water.png"
  description: Compatible
- img: "../../img/iconos/usb gabo.png"
  description: 3.0/2.0
- img: "../../img/iconos/fans.png"
  description: 3 RGB FAN
- img: "../../img/iconos/vidriotemplado.png"
  description: Vidrio templado

---
#### Compatibilidad:

* Entrada micrófono / auriculares
* Filtro Antipolvo
* USB 3.2 X 2
* USB 2.0 X 2
* USB Tipo C x 1
* Vidrio templado
* Compatible con SSD
* Placas de video de 430mm
* Medidas: alto 550cm, largo 480cm, ancho 225cm
* Refrigeración líquida superior: 1 x 360 / 240mm
* Refrigeración frontal: 1 x 360 / 240mm
* Refrigeración líquida lateral: 1 x 240mm
* Posterior: 120 mm
* Límite de CPU: Máx 165 mm
* Tipo de fuente: ATX
* Filtro de la fuente: Sí (Encastrable)
* Filtro Frontal: No
* Posición de la fuente: Inferior
* Vidrio frontal: SI
* Vidrio lateral izquierdo: SI
* Vidrio lateral derecho: SI
* Vidrio superior: SI
* Frigorífico frontal: 2 x 200 mm (RGB instalado)
* Refrigerador superior: 2 x 200mm (no incluidos)
* Nevera inferior: 2 x 120mm (no incluidos)
* Frío posterior: 1 x 120 / 140mm (RGB 120mm incluido)
* Disco duro: 2
* SSD: 4/6
* Base: pies dobles
* Led frontal: NO
* Controlador RGB y control remoto