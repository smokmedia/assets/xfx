---
title: GC-06B
tipo: gabinete
modelo: keiko
tags:
- keiko
categories:
- gabinete
mostrar: si
carrusel:
- imga: "../../img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06.jpg"
  numero: "1"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06-1.jpg"
  numero: "2"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06-2.jpg"
  numero: "3"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06-3.jpg"
  numero: "4"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06-5.jpg"
  numero: "5"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06-4.jpg"
  numero: "6"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06-6.jpg"
  numero: "7"
- imga: "../../img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06-7.jpg"
  numero: "8"
imagenes:
- ruta: "../img/gabinete_slider.jpg"
- ruta: "../img/gabinete_slider.jpg"
descripcion: Esta línea de gabinetes fue creada con la inspiración de la armadura
  Samurái que lleva su nombre. Mediante la utilización de nobles materiales, combinados
  entre sí, logramos crear un gabinete resistente que se adapta perfectamente a cualquier
  uso, y proporciona una seguridad óptima a los diferentes componentes de hardware
  de tu sistema. La armadura Keiko es una armadura lamelar pre Samurái utilizada en
  Japón alrededor del siglo V (Era Sengoku). Compuesta por una gran cantidad de piezas
  de cuero laqueado y/o hierro entrelazadas, y unidas por cuerda en tiras horizontales.
  Este tipo de armadura se utilizó en el ejército durante el Japón feudal por proporcionar
  una gran protección a su portador.
imgf: "/img/productos/gabinetes/keiko/gabinete GC06 keiko/KeikoGC06portad.png"
iconos:
- img: "../../img/iconos/aire.png"
  description: MAX-165mm
- img: "../../img/iconos/filtro.png"
  description: Filtro antipolvo
- img: "../../img/iconos/placadevideo.png"
  description: MAX-305mm
- img: "../../img/iconos/sonido.png"
  description: Jacks 3.5mm
- img: "../../img/iconos/ssd.png"
  description: Compatible
- img: "../../img/iconos/water.png"
  description: Compatible
- img: "../../img/iconos/usb gabo.png"
  description: 3.0/2.0
- img: "../../img/iconos/fans.png"
  description: 8 FAN Slot
- img: "../../img/iconos/vidriotemplado.png"
  description: Vidrio templado

---
#### Compatibilidad:

* Entrada micrófono / auriculares
* Filtro Antipolvo
* USB 2.0 / 3.0
* Vidrio templado
* Compatibilida de disco: 1 SSD y 1/2 HDD
* Placas de video de 305mm
* Refrigeración líquida: Posterior de 120mm Superior de 120mm / 240mm
* Límite de CPU: máximo 165 mm
* Tipo de fuente: ATX
* Filtro antipolvo superior: Sí (Magnético)
* Filtro de la fuente: Sí (Encastrable)
* Filtro Frontal: No
* Posición de la fuente: Inferior
* Frigorífico frontal: 3 x 120 mm (no incluidos)
* Nevera inferior: 2 x 120mm (no incluidos)
* Refrigerador posterior: 1 x 120 mm (no incluido)
* Refrigerador superior: 2 x 120/140 mm (no incluidos)
* Vidrio Frontal: No
* Vidrio lateral: Sí (Con apoyo antivibratorio)
* Base 4 Pines antideslizantes