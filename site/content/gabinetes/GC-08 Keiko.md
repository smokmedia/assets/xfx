---
title : "GC-08"
tipo : "gabinete"
modelo : "keiko"
tags : ["keiko"]
categories : ["gabinete"]

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

carrusel :
 - imga : "../../img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08.jpg"
   numero : "1"

 - imga : "../../img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08-1.jpg"
   numero : "2"

 - imga : "../../img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08-2.jpg"
   numero : "3"

 - imga : "../../img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08-3.jpg"
   numero : "4"

 - imga : "../../img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08-4.jpg"
   numero : "5"

 - imga : "../../img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08-5.jpg"
   numero : "6"

 - imga : "../../img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08-6.jpg"
   numero : "7"

 - imga : "../../img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08-7.jpg"
   numero : "8"

imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "Esta línea de gabinetes fue creada con la inspiración de la armadura Samurái que lleva su nombre. Mediante la utilización de nobles materiales, combinados entre sí, logramos crear un gabinete resistente que se adapta perfectamente a cualquier uso, y proporciona una seguridad óptima a los diferentes componentes de hardware de tu sistema.
La armadura Keiko es una armadura lamelar pre Samurái utilizada en Japón alrededor del siglo V (Era Sengoku). Compuesta por una gran cantidad de piezas de cuero laqueado y/o hierro entrelazadas, y unidas por cuerda en tiras horizontales. Este tipo de armadura se utilizó en el ejército durante el Japón feudal por proporcionar una gran protección a su portador."

imgf : "/img/productos/gabinetes/keiko/gabinete GC08 Keiko/KeikoGC08portada.png"

iconos:
 - img : "../../img/iconos/aire.png"
   description : "MAX-165mm"
 - img : "../../img/iconos/filtro.png"
   description : "Filtro antipolvo"
 - img : "../../img/iconos/placadevideo.png"
   description : "MAX-320mm"
 - img : "../../img/iconos/sonido.png"
   description : "Jacks 3.5mm"
 - img : "../../img/iconos/ssd.png"
   description : "Compatible"
 - img : "../../img/iconos/water.png"
   description : "Compatible"
 - img : "../../img/iconos/usb gabo.png"
   description : "3.0/2.0"
 - img : "../../img/iconos/fans.png"
   description : "4 RGB FAN"
 - img : "../../img/iconos/vidriotemplado.png"
   description : "Vidrio templado"
   
---
#### Compatibilidad: ####
- Entrada micrófono/auriculares
- Filtro Antipolvo
- USB 2.0/3.0
- Vidrio templado
- SSD compatible
- Placas de video de 320mm
- Refrigeración líquida
- CPU limit: Máx 165 mm
- 4 Ventiladores RGB
- 8 Slot para ventiladores
- Medidas: 366cm de largo, 210cm ancho, 452cm alto
- HDD 2
- SSD 2
- Cooler frontal: 3 x 120mm (RGB instalados)
- Cooler posterior: 1 x 120mm (RGB instalado)
- Cooler superior: 2 x 120mm (no incluidos)
- Cooler inferior: 2 x 120mm (no incluidos)
- Refrigeración líquida superior: 120/240mm
- Refrigeración líquida posterior: 120mm