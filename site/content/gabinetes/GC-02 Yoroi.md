---
title: GC-02
tipo: gabinete
modelo: yoroi
tags:
- yoroi
categories:
- gabinete
mostrar: 'no'
carrusel:
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-02 yoroi/YoroiGC02.jpg"
  numero: "1"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-02 yoroi/YoroiGC02-1.jpg"
  numero: "2"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-02 yoroi/YoroiGC02-3.jpg"
  numero: "3"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-02 yoroi/YoroiGC02-4.jpg"
  numero: "4"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-02 yoroi/YoroiGC02-5.jpg"
  numero: "5"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-02 yoroi/YoroiGC02-6.jpg"
  numero: "6"
- imga: "../../img/productos/gabinetes/yoroi/gabinete GC-02 yoroi/YoroiGC02-7.jpg"
  numero: "7"
imagenes:
- ruta: "../img/gabinete_slider.jpg"
- ruta: "../img/gabinete_slider.jpg"
descripcion: Las armaduras Yoroi fueron utilizadas durante el Periodo Edo (posterior
  al Sengoku, período de Guerra). Esta armadura era portada por los Samuráis de alto
  rango, proporcionando la misma protección que las Keiko, estas se caracterizaban
  por ser más ligeras y brindaban mayor libertad de movimiento a quien la usara, a
  su vez, dependiendo del status a cual perteneciera el Samurái, su ornamenta y colores
  eran distintos, eso era una de las formas para diferenciar a cada clan.
imgf: "/img/productos/gabinetes/yoroi/gabinete GC-02 yoroi/YoroiGC02portada.png"
iconos:
- img: "../../img/iconos/aire.png"
  description: MAX-165mm
- img: "../../img/iconos/filtro.png"
  description: Filtro antipolvo
- img: "../../img/iconos/placadevideo.png"
  description: MAX-305mm
- img: "../../img/iconos/sonido.png"
  description: Jacks 3.5mm
- img: "../../img/iconos/ssd.png"
  description: Compatible
- img: "../../img/iconos/water.png"
  description: Compatible
- img: "../../img/iconos/usb gabo.png"
  description: 3.0/2.0
- img: "../../img/iconos/fans.png"
  description: 3 RGB FAN
- img: "../../img/iconos/vidriotemplado.png"
  description: Vidrio templado

---
Gabinetes de alta gama inspirados en la armadura Samurái que lleva su nombre (Yoroi). Ésta línea de productos es la evolución de la gama Keiko. Además de poseer la protección necesaria para el hardware de tu PC, poseen estéticas que los únicos hacen y singulares para adaptarse al gusto del usuario.

#### Compatibilidad:

* Entrada micrófono / auriculares
* Filtro Antipolvo
* USB 2.0 / 3.0
* Vidrio templado
* Compatible con 1 SSD y 2 HDD
* Placas de video de 305mm
* Refrigeración líquida 120 / 240mm
* Límite de CPU: Máx 165 mm
* Tipo de fuente: ATX
* Posición de la fuente: Inferior
* Enfriador frontal 3 x 120mm (Incluidos - Con RGB no controlable)
* Refrigerador inferior: 2 x 120 mm (No incluido)
* Refrigerador posterior: 1 x 120 mm (No incluido)
* Refrigerador superior: 2 x 120 mm (No incluido)
* Filtro inferior: Si - Encastrable (Fuente)
* Vidrio Frontal: Si (Con apoyo antivibratorio)
* Vidrio lateral: Sí (Con apoyo antivibratorio)
* Base 4 Pines antideslizantes
* Filtro antipolvo superior: Sí (Magnético)
* Filtro de la fuente: Sí (encastrable)