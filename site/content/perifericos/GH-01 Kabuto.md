---
title: GH-01
tipo: periferico
tags:
- mouse
categories:
- perifericos
mostrar: si
carrusel:
- imga: "../../img/productos/perifericos/Auriculares/Auriculares GH-01 Kabuto/KabutoGH01.jpg"
  numero: "1"
- imga: "../../img/productos/perifericos/Auriculares/Auriculares GH-01 Kabuto/KabutoGH01-1.jpg"
  numero: "2"
- imga: "../../img/productos/perifericos/Auriculares/Auriculares GH-01 Kabuto/KabutoGH01-2.jpg"
  numero: "3"
- imga: "../../img/productos/perifericos/Auriculares/Auriculares GH-01 Kabuto/KabutoGH01-3.jpg"
  numero: "4"
- imga: "../../img/productos/perifericos/Auriculares/Auriculares GH-01 Kabuto/KabutoGH01-4.jpg"
  numero: "5"
imagenes:
- ruta: "../img/gabinete_slider.jpg"
- ruta: "../img/gabinete_slider.jpg"
descripcion: 'Es el nombre que se le da al casco Samurái. Este tenía dos usos principales;
  además de ser usado como protección en las batallas, tenía un uso ceremonial. Los
  materiales utilizados en las ornamentas fueron las piedras y metales preciosos trabajados
  de manera artesanal por grandes maestros. Sus propiedades para el uso en las batallas
  proporcionaba la protección de la cabeza frente a los ataques cuerpo a cuerpo o
  a distancia. Construido con cueros y placas de latón, posibilitaba que fuera duro
  pero también liviano y cómodo. Los auriculares Kabuto de XFX, son tu mejor opción
  para sentirte protegido durante tus partidas potenciando tu sentido de la audición,  brindándote
  un sonido alta fidelidad envolvente gracias a su tecnología de 7.1 software, la
  que te permitirá sentir lo que sucede a tu alrededor en todas las direcciones. Creado
  con materiales resistentes y cómodos, te proveerán de una experiencia única para
  que te sumerjas en el juego. '
imgf: "/img/productos/perifericos/Auriculares/Auriculares GH-01 Kabuto/KabutoGH01portada.png"
iconos:
- img: "../../img/iconos/7.1.png"
  description: Sonido
- img: "../../img/iconos/mute.png"
  description: Botón Mute
- img: "../../img/iconos/vol.png"
  description: Vol. control
- img: "../../img/iconos/usb auris.png"
  description: Cable
- img: "../../img/iconos/rgb.png"
  description: 16m colores

---
Nuestros auriculares para juegos con sonido envolvente 7.1 mejorado por vibraciones de graves te brindarán una experiencia y una atmósfera de juego impresionante. Escucharás los pasos del enemigo o los movimientos de tus enemigos con un posicionamiento preciso y estarás listo para defender o sabotear sus acciones desde las direcciones correctas. La posición del micrófono se puede ajustar mediante un cuello de cisne flexible para garantizar una comunicación de voz perfecta y clara. Control de volumen y botón de silencio del micrófono diseñado en la cubierta del auricular. Soporte de metal para auriculares nivel de producto mejorado. Nuestros auriculares para juegos con apariencia refinada y materiales perfectos. La maravillosa luz RGB define su estilo de juego y hace que los auriculares sean visibles en un entorno oscuro.

#### Caracteristicas:

* Auriculares para juegos 7.1 con luz RGB, calidad de sonido envolvente
* Con software delicado 7.1
* Impedancia: 16Ω +/- 15%
* Sensibilidad del altavoz: 110 +/- 3db
* Rango de frecuencia: 20Hz-20KHz
* Conductor: 50 mm * 2
* Sensibilidad de micrófono: -42 ± 3dB
* Rango de frecuencia de micrófono: 20-20KHz
* Tipo de micrófono: OMNI direccional
* Cable de 2 metros en material PVC negro
* Dimensiones: 200 * 100 * 220 mm
* Peso: 352g
* Temperatura de funcionamiento: -10ºC a + 45ºC
* Humedad de funcionamiento: 40 a 85% RH
* Temperatura de almacenamiento: -10ºC a + 45ºC
* Humedad de almacenamiento: 40 a 85% RH