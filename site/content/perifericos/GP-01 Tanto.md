---
title : "GP-01"
tipo : "periferico"
tags : ["mouse"]
categories : ["perifericos"]
type : "perifericos"

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

carrusel :
 - imga : "../../img/productos/perifericos/Pads/Pad GP-01 Tanto/TantoPad.jpg"
   numero : "1"

 - imga : "../../img/productos/perifericos/Pads/Pad GP-01 Tanto/TantoPad-1.jpg"
   numero : "2"

 - imga : "../../img/productos/perifericos/Pads/Pad GP-01 Tanto/TantoPad-2.jpg"
   numero : "3"

 - imga : "../../img/productos/perifericos/Pads/Pad GP-01 Tanto/TantoPad-3.jpg"
   numero : "4"

 - imga : "../../img/productos/perifericos/Pads/Pad GP-01 Tanto/TantoPad-4.jpg"
   numero : "5"


imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "Tanto es un arma de filo pequeña similar a un puñal. Similar a una Katana podría denominarse como la versión miniatura de esta aunque presentan diferencias en la empuñadura y la guarda. Era utilizado en la batalla como complemento de la katana que con movimientos combinados podían lograr desarmar al oponente. 
Otro de los usos del tanto era ceremonial (el harakiri: suicidio que el samurai realizaba con el fin de recobrar su honor tras una deshonra) y es por esto que su estética y diseño eran más ornamentados. "

imgf : "/img/productos/perifericos/Pads/Pad GP-01 Tanto/TantoPadportada.png"

iconos:
- img: "../../img/iconos/wear.png"
  description: Anti-Wear Seam
- img: "../../img/iconos/soft.png"
  description: Movimiento Suave
- img: "../../img/iconos/slip.png"
  description: Anti-Slip
- img: "../../img/iconos/dimension.png"
  description: Dimensiónes extremas
- img: "../../img/iconos/fast.png"
  description: Respuesta rápida
- img: "../../img/iconos/spill.png"
  description: Anti-Spill

---
Para presentar batalla, nada mejor que una segunda arma, un complemento imprescindible para tu mouse. Con la estética y el diseño que caracterizan a XFX, nuestro mousepad es esencial para lograr movimientos fluidos que permite proporcionar una mayor comodidad en las partidas largas.

#### 900x400x3mm ####
Espacio extremo para mayor rango de movimiento

#### Constura anti desgaste ####
- Costura anti desgaste para largas sesiones de uso.

#### Anti derrame ####
- Material anti derrame de líquidos.

#### Movimiento suave ####
- Tejido especial para movimientos más suaves y fluidos.

#### Dimensiones extremas ####
- Espacio extremo para mayor rango de movimiento (900x400x3mm)

#### Movimiento rápido ####
- Textura específica para mayor velocidad de desplazamiento del mouse.

#### Base anti deslizamiento ####
- Base anti desplazamiento para mayor precisión en el juego.