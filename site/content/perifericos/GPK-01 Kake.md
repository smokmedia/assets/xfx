---
title: GKP-01
tipo: periferico
tags:
- mouse
categories:
- perifericos
mostrar: si
carrusel:
- imga: "../../img/productos/perifericos/Kits/KitGPK-01/KakeKIT.jpg"
  numero: "1"
- imga: "../../img/productos/perifericos/Kits/KitGPK-01/KakeKIT-1.jpg"
  numero: "2"
- imga: "../../img/productos/perifericos/Kits/KitGPK-01/KakeKIT-2.jpg"
  numero: "3"
- imga: "../../img/productos/perifericos/Kits/KitGPK-01/KakeKIT-3.jpg"
  numero: "4"
imagenes:
- ruta: "../img/gabinete_slider.jpg"
- ruta: "../img/gabinete_slider.jpg"
descripcion: 'Kake: En la época del Japón Feudal, dentro de los Dojos samurái, existían  espacios
  destinados para el Kake que funcionaban como un repositorio donde el samurái dejaba
  sus armas cuando no estaban en combate. Junto al Kake se hallaba el altar donde
  los samurái depositaban sus armaduras al desprenderse de ellas. Tanto el kake como
  el altar formaban parte del templo donde los samuráis rendían tributo y rezaban
  a su equipamiento. Agradeciéndoles por su protección y desempeño en la batalla tenían
  la creencia de que tanto las armas como las armaduras poseían sus propias almas.'
imgf: "/img/productos/perifericos/Kits/KitGPK-01/KakeKITportada.png"

---
Kake Combo periféricos: Así como el samurái reunía todo su equipamiento en un solo lugar, XFX reúne en un solo conjunto todas sus armas para que te destaquen entre tus aliados y derrotes a tus adversarios en la batalla. No necesitas nada más, vas a sentirlos como una extensión de tu propio cuerpo e inmediatamente depositarás tu alma en ellos para que puedas lograr una sinergia que te convierta en un contenido sin igual.

#### Ratón

* Mouse óptico USB para juegos
* Resolución: 800/1600/2400/3200
* Chip: NST5312
* Botones: 7
* Desplazamiento LED antideslizante de goma
* Acabado de la carcasa de revestimiento de goma
* Logotipo de luz OEM
* Modos de luz: luz de respiración de 7 colores
* Cable de longitud: 1,5 metros de cable de PVC
* Conéctalo y juega
* Tamaño: 122 * 69 * 40 mm
* Requisitos del sistema: Winows98 / 2000 / ME / XP / VISTA / Win7 / 8/10 / MAC
* Auriculares

#### Auriculares:

* Conductor: 40 mm
* Impedancia: 16Ω +/- 15%
* Sensibilidad: 108dB +/- 3dB

#### Micrófono:

* Rango de frecuencia: 20 Hz \~ 20 KHz
* Sensibilidad: -42dB +/- 3dB
* Impedancia: 2,2 KΩ
* Tamaño del micrófono: Ø6.5 * 5.0MM
* Longitud del cable: 2,1 metros
* Luz RGB
* Conector: 2 * 3,5 ”para audio + USB para luz, con adaptador de cable PS4 adicional
* Requisitos del sistema: Winows98 / 2000 / ME / XP / VISTA / Win7 / 8/10 / MAC

#### Almohadilla de juego

* Costura anti desgaste para largas sesiones de uso
* Material anti derrame de líquidos
* Tejido especial para movimientos más suaves y fluidos
* Espacio extremo para mayor rango de movimiento (227x190x3mm)
* Textura específica para mayor velocidad de desplazamiento del mouse
* Base anti desplazamiento para mayor precisión en el juego

#### Teclado

* Fabricado con material ABS original
* Teclas de impresión láser, sin decoloración independientemente del tiempo que haya utilizado
* Admite retroiluminación de arco iris y luz de teclas
* Cable de PU sólido de 1,5 metros con rendimiento estable
* Diseño a prueba de agua
* “Conecta y reproduce”; no se requiere conductor
* Vida útil por más de 10,000,000 clicks.
* Interfaz en español
* Tamaño: 432x127x26 mm
* Peso: 430 ± 5 g.