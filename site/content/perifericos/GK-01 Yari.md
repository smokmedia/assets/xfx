---
title : "GK-01"
tipo : "periferico"
tags : ["mouse"]
categories : ["perifericos"]
type : "perifericos"

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

carrusel :
 - imga : "../../img/productos/perifericos/Teclados/Teclado GK-01 Yari/yari.jpg"
   numero : "1"

 - imga : "../../img/productos/perifericos/Teclados/Teclado GK-01 Yari/Yari-1.jpg"
   numero : "2"

 - imga : "../../img/productos/perifericos/Teclados/Teclado GK-01 Yari/Yari-2.jpg"
   numero : "3"

 - imga : "../../img/productos/perifericos/Teclados/Teclado GK-01 Yari/Yari-3.jpg"
   numero : "4"
   
 - imga : "../../img/productos/perifericos/Teclados/Teclado GK-01 Yari/Yari-4.jpg"
   numero : "5"
   
 - imga : "../../img/productos/perifericos/Teclados/Teclado GK-01 Yari/Yari-5.jpg"
   numero : "6"  

 - imga : "../../img/productos/perifericos/Teclados/Teclado GK-01 Yari/Yari-6.jpg"
   numero : "7"

 - imga : "../../img/productos/perifericos/Teclados/Teclado GK-01 Yari/Yari-7.jpg"
   numero : "8"

imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "El Yari es una lanza utilizada por la milicia japonesa y los samuráis en los campos de guerra. Si bien los Samuráis están estrechamente relacionados al arte del uso de la espada (katana) también estaban entrenados y poseían una excelente destreza en el uso del arco y la lanza. 
El Yari poseía una gran ventaja gracias a su longitud lo que le permitía un alcance de ataque mayor y al mismo tiempo su peso tan ligero posibilitaba  una mayor velocidad de ataque."

imgf : "/img/productos/perifericos/Teclados/Teclado GK-01 Yari/yariportada.png"

iconos:
- img: "../../img/iconos/mecanico.png"
  description: Mecánico
- img: "../../img/iconos/español.png"
  description: Español
- img: "../../img/iconos/repair.png"
  description: Repair Kit
- img: "../../img/iconos/backlight.png"
  description: RGB Backlight

---
Es el arma perfecta para tu campo de batalla. La cualidad mecánica de las teclas te permitirá moverte con agilidad y precisión. Su ligereza, durabilidad y fuerza se adaptan a las luchas más arduas sin ningún esfuerzo. Sentirás el control absoluto.

#### Caracteristicas:

* Luz de fondo arcoiris
* Kit de reparación
* Mecánico
* Interfaz en español
* Blue Switch, 50 millones de clicks
* Cable USB de 1,5 M con anillo magnético
* Voltaje de funcionamiento: USB 5 V
* Fuerza de funcionamiento: 60 g ± 10 g
* Distancia total: 4.0 ± 0.5 mm
* Conecta y reproduce
* Dimensión: 449 * 182 * 41 mm
* Peso: 897 ± 20g
* Compatible con: Windows XP, Vista, Win7, Win8, Win10A, Android, Linux, Max y Mac
* Luz de fondo: se activa presionando la tecla de iluminación
* Sitema anti-fantasma