---
title : "GM-01"
tipo : "periferico"
tags : ["mouse"]
categories : ["perifericos"]
type : "perifericos"

#Corroboracion para el final de cada producto aparezca o no
mostrar : "si"

carrusel :
 - imga : "../../img/productos/perifericos/Mouse/GM-01/KunaiGM01.jpg"
   numero : "1"

 - imga : "../../img/productos/perifericos/Mouse/GM-01/KunaiGM01-1.jpg"
   numero : "2"

 - imga : "../../img/productos/perifericos/Mouse/GM-01/KunaiGM01-3.jpg"
   numero : "3"

 - imga : "../../img/productos/perifericos/Mouse/GM-01/KunaiGM01-4.jpg"
   numero : "4"

 - imga : "../../img/productos/perifericos/Mouse/GM-01/KunaiGM01-5.jpg"
   numero : "5"

 - imga : "../../img/productos/perifericos/Mouse/GM-01/KunaiGM01-6.jpg"
   numero : "6"

imagenes :
 - ruta : "../img/gabinete_slider.jpg"
 - ruta : "../img/gabinete_slider.jpg"

descripcion : "Su origen data del periodo Sengoku y originalmente fue concebido como una herramienta para la agricultura y la carpintería en el Japón feudal, pero gracias a su pequeño tamaño y a su forma de cuchilla triangular fue rápidamente adaptado como un arma perfecta para apuñalar, y gracias a que poseía un excelente balance y su diseño geométrico podía utilizarse también como arma arrojadiza."

imgf : "/img/productos/perifericos/Mouse/GM-01/gm01portada.png"

iconos:
 - img : "../../img/iconos/switch.png"
   description : "Huano"
 - img : "../../img/iconos/rgb.png"
   description : "16m colores"
 - img : "../../img/iconos/sensor.png"
   description : "Repair Kit"
 - img : "../../img/iconos/backlight.png"
   description : "RGB Backlight"
   
   
---

Dentro del mundo del Gaming es indispensable y tu victoria o derrota estarán estrechamente ligadas a tu destreza en su uso.  Es por eso que tomamos los atributos del Kunai en cuanto a su forma y diferentes usos para crear un mouse de un diseño ergonómico que se adapta a la perfección a tu uso. Así como los guerreros samurái utilizaban el Kunai para sus movimientos y destrezas, nuestro mouse te permite realizar movimientos precisos para acercarte a una victoria asegurada.

#### Características: ####
- Switch Huano
- PixArt 3325
- 10.000 DPI
- Luz RGB
- Software personalizado
- Resolución: hardware 5000DPI, software 10000DPI
- 8 botones: (L) Tecla izquierda, (M) Tecla central, (R) Tecla derecha, (K5) Adelante, (K4) Backwared, DPI + Tecla, DPI - Tecla, Tecla de disparo
- Cable: cable TPE de 1,8 metros con imán
- 100 ips
- Aceleración: 20g
- FPS: máximo 4000
- Desplazamiento de goma antideslizante
- Dimensión: 126,3 (L) * 67,7 (W) * 42,5 (H) mm
- Peso: 120 g (± 5 g)
- Voltaje / corriente de trabajo: 5V / 100mA
- Temperatura / humedad de trabajo: 0 °C ~ +40 °C / 0-90% RH
- Requisitos del sistema: Winows98 / 2000 / ME / XP / VISTA / Win7 / 8/10 / MAC
